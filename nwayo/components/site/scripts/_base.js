//-------------------------------------
//-- Site - Base
//-------------------------------------

(() => {
	'use strict';

	const local = {};


	//-- Cache data instantly
	local.cache = () => {
		// Env
		// app.env.isUniquePage = app.env.pageId === 'UID';
		// app.env.isTypePage   = _.includes(app.env.pageTags, 'TYPE-ID');
		// app.env.isOneOfThese = !!_.intersection(app.env.pageTags, ['ID1', 'ID2']).length;

	};


	//-- Cache data once DOM is loaded
	local.cacheDOM = () => {

		//

	};


	//-- Bind events once DOM is loaded
	local.bind = () => {

		//

	};


	//-- Subscribe to topics
	local.subscribe = () => {
		// pinki.message.subscribe('foo.bar',  () => {});
	};


	//-- Execute once DOM is loaded
	local.start = () => {
		local.PortfolioRedirectToProjectUrl();
		local.addActiveStatePaypalInput();
	};


	//-- Execute once page is loaded
	local.delayedStart = () => {
		//
	};

	local.PortfolioRedirectToProjectUrl = () => {
		const projectItem = $('.gabick-project-custom-hover');
		projectItem.on('click', (e) => {
			e.preventDefault();
			const projectUrl = $(e.currentTarget).find('a').first().attr('href');
			if (projectUrl !== null) {
				global.window.open(projectUrl, '_blank');
			}
		});
	};

	local.addActiveStatePaypalInput = () => {
		const inputClicked = $('.donate-amount').next('label');

		inputClicked.on('click', (e) => {
			inputClicked.removeClass('active').next('label');
			$(e.currentTarget).addClass('active');
		});
	};

	// Outline
	local.cache();
	local.subscribe();

	// DOM Ready
	pinki.vow.when(DOM_PARSED).then(() => {
		local.cacheDOM();
		local.bind();
		local.start();
	});

	// Document loaded
	pinki.vow.when(DOCUMENT_LOADED).then(() => {
		local.delayedStart();
	});

})();
