<?php
AGSLayouts::VERSION; // Access control

class AGSLayoutsSiteImporter {
	private static $curl, $importPostId;
	
	static function onInit() {
		// wp-layouts-server/ags-layouts-server.php
		register_post_type('wplayouts-siteimport', array(
			'labels' => array(
				'name' => 'WP Layouts Site Imports',
				'singular_name' => 'WP Layouts Site Import'
			),
			'public' => false,
			'can_export' => false,
			'supports' => array(
				
			),
		));
		
	}
	
	static function importExtraData($postId, $importLocation, $extraData) {
		if ( isset($extraData['widgets']) ) {
			update_post_meta($postId, '_ags_layouts_widgets', wp_slash($extraData['widgets']) );
		} else {
			delete_post_meta($postId, '_ags_layouts_widgets');
		}
		
		if ( isset($extraData['caldera_forms']) ) {
			update_post_meta($postId, '_ags_layouts_caldera_forms', wp_slash($extraData['caldera_forms']) );
		} else {
			delete_post_meta($postId, '_ags_layouts_caldera_forms');
		}
		
		if ( isset($extraData['agsxto']) ) {
			update_post_meta($postId, '_ags_layouts_agsxto', wp_slash($extraData['agsxto']) );
		} else {
			delete_post_meta($postId, '_ags_layouts_agsxto');
		}
		
		if ( isset($extraData['config']) ) {
			update_post_meta($postId, '_ags_layouts_config', wp_slash($extraData['config']) );
		} else {
			delete_post_meta($postId, '_ags_layouts_config');
		}
		
	}
	
	static function getImportPostId($userId=null, $multiple = false) {
		if (empty(self::$importPostId)) {
			if (!$userId) {
				$userId = get_current_user_id();
			}
			$ids = get_posts(array(
				'author' => $userId,
				'post_type' => 'wplayouts-siteimport',
				'post_status' => 'private',
				'posts_per_page' => 1,
				'fields' => 'ids'
			));
			self::$importPostId = empty($ids) ? self::createImportPost() : $ids[0];
		}
		return self::$importPostId;
	}
	
	static function getAllImportPostIds() {
		self::$disablePostsFilter = true;
		$previewPageIds = get_posts(array(
			'post_type' => 'wplayouts-siteimport',
			'post_status' => 'private',
			'nopaging' => true,
			'fields' => 'ids'
		));
		self::$disablePostsFilter = false;
		return $previewPageIds;
	}
	
	static function createImportPost() {
		$importPostId = wp_insert_post(array(
			'post_type' => 'wplayouts-siteimport',
			'post_status' => 'private'
		));
		
		if ($importPostId && is_numeric($importPostId)) {
			return $importPostId;
		}
	}
	
	static function onUserDeleted($deletedUserId) {
		wp_delete_post(self::getImportPostId($deletedUserId), true);
	}
	
	// Hooked in main plugin file
	static function onPluginDeactivate() {
		foreach (self::getAllImportPostIds() as $postId) {
			wp_delete_post($postId, true);
		}
	}
	
}


add_action('init', array('AGSLayoutsSiteImporter', 'onInit'), 99);
add_action('deleted_user', array('AGSLayoutsSiteImporter', 'onUserDeleted'));
add_action('remove_user_from_blog', array('AGSLayoutsSiteImporter', 'onUserDeleted'));

include( __DIR__.'/../../includes/site-import/functions.php' );