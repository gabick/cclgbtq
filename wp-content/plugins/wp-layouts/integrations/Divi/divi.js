/*
This file includes code based on parts of the Divi theme and/or the
Divi Builder by Elegant Themes. See credit and licensing info in ags-layouts.php
(reproduced below with modification(s)).

This plugin includes code based on parts of the Divi theme and/or the
Divi Builder, copyright Elegant Themes, released under GPLv2, licensed under
GPLv3 for this project by special permission (see ../../license/divi-CREDITS.md
for credits applicable to Divi, and ../../license/license.txt for GPLv3 text).
*/

jQuery(document).ready(function($) {

	var ags_layouts_api_url = ags_layouts_admin_config.wpAjaxUrl,
		isThemeBuilder = document.getElementById('et-theme-builder'),
		currentLayoutSlotTemplate, currentLayoutSlotType;
	
	// Check for old backend builder
	var oldBackendBuilderMessage = 'It looks like you are using the old version of the Divi backend builder, which is not compatible with WP Layouts. If you would like to use WP Layouts with Divi, please ensure you are using an up-to-date version of Divi, then enable the new Divi builder via the link below the editor or in Divi settings.';
	/* This line is based on the Divi theme (see copyright and licensing details at the top of this file) - modified by Jonathan Hall 2019-07-04 */ var oldBackendBuilderTabsSelector = '[data-open_tab="et-pb-ags-layouts-tab"],[data-open_tab="et-pb-ags-layouts-my-tab"]';
	$(document.body).on('click', oldBackendBuilderTabsSelector, function() {
		ags_layouts_message_dialog('', oldBackendBuilderMessage);
	});
	
	$(document.body).on('click', '.et-tb-layout-slot', function() {
	
		currentLayoutSlotTemplate = $(this).closest('.et-tb-template-list>*').index();
		
		switch ( $(this).index() ) {
			case 0:
				currentLayoutSlotType = 'header';
				break;
			case 1:
				currentLayoutSlotType = 'body';
				break;
			case 2:
				currentLayoutSlotType = 'footer';
				break;
		}
		
	});
	
	ags_layouts_onCreateElementWithId('et_pb_layout_controls', function(layoutControls) {
		$('<p>')
			.addClass('ags-layouts-divi-not-supported ags-layouts-notification ags-layouts-notification-error')
			.text(oldBackendBuilderMessage)
			.insertBefore('#et_pb_layout');
	});
	
	ags_layouts_register_extra_task('Saving existing content', function(doneCallback) {
		
		var fbApp = document.getElementById('et-fb-app');
		
		if ( isThemeBuilder && !fbApp ) {
			
			var $saveButton = $('.et-tb-admin-save-button');
			if ($saveButton.length && currentLayoutSlotType) {
				ags_layouts_onElementClassAdded($saveButton[0], 'et-tb-admin-save-button--success', function() {
					$.get(ags_layouts_api_url, {
						action: 'ags_layouts_get_tb_id',
						template: currentLayoutSlotTemplate,
						type: currentLayoutSlotType
					}, function(tbId) {
					
						if (tbId && tbId.success && tbId.data) {
							window.ags_layouts_admin_config.editingPostId = tbId.data;
							doneCallback();
						}
						
					}, 'json');
				});
				$saveButton.click();
			}
			
		} else if ( document.getElementById('wpbody') && !isThemeBuilder ) {
		
			$('#et-fb-settings-column').remove();
			var $postForm = $('#post');
			var postFormSubmitHandler = function() {
				$postForm.off('submit', postFormSubmitHandler);
				doneCallback();
				return false;
			};
			$postForm.on('submit', postFormSubmitHandler);
			
			$('#save-post:visible').click().length || $('#publish').click();
			
			setTimeout(function() {
				$('.et-fb-preloader').remove();
			}, 500);
			
		} else {
		
			var $saveButton = $('.et-fb-button--save-draft:first');
			
			if (!$saveButton.length) {
				$saveButton = $('.et-fb-button--publish:first');
			}
			
			var $saveButtonIcon = $saveButton.find('.et-fb-icon:first');
			
			
			if ($saveButtonIcon.length) {
				ags_layouts_onElementClassAdded($saveButtonIcon[0], 'et-fb-icon--check', function() {
					if (isThemeBuilder) {
						var frame = $(fbApp).find('iframe:first');
						if ( frame.length && frame[0].contentWindow && frame[0].contentWindow.ETBuilderBackendDynamic && frame[0].contentWindow.ETBuilderBackendDynamic.postId) {// all good
							window.ags_layouts_admin_config.editingPostId = frame[0].contentWindow.ETBuilderBackendDynamic.postId; // all good
							
							var frameSrc = frame.attr('src');
							frame
								.attr('src', 'about:blank')
								.siblings().remove();
							ags_layouts_register_extra_task('Reloading...', function(doneCallback) {
								frame.attr('src', frameSrc);
							}, 'divi', 'import', 'after');
						}
					}
					
					doneCallback();
				});
				$saveButton.click();
			}
		}
		
	}, 'divi', 'import', 'before');
	
	

	// Visual builder
	$('body:first').on('click', '#et-fb-app .et-fb-modal-settings--library .et-fb-settings-tabs-nav-item', function() {
		var $tab = $(this);
		var $modal = $tab.closest('.et-fb-modal-settings--library');
		var $modalTabContents = $modal.find('.et-fb-library-container:first');
		
		
		var contentsCb = isThemeBuilder ? function(){} : null;
		
		if ($tab.hasClass('et-fb-settings-options_tab_ags-layouts')) {
			$modalTabContents.children().hide();
			var $container = $('<div>').addClass('ags-layouts-dialog-container').appendTo($modalTabContents);
			ags_layout_import_ui('divi', $container, 'layouts_ags', contentsCb);
		} else if ($tab.hasClass('et-fb-settings-options_tab_ags-layouts-my')) {
			$modalTabContents.children().hide();
			var $container = $('<div>').addClass('ags-layouts-dialog-container').appendTo($modalTabContents);
			ags_layout_import_ui('divi', $container, 'layouts_my', contentsCb);
		} else {
			$modalTabContents.children('.ags-layouts-dialog-container').remove();
			$modalTabContents.children().show();
		}
	});
	
	// Theme builder
	$('body:first').on('click', '.et-tb-admin-modals-portal .et-tb-divi-library-modal .et-common-tabs-navigation__button', function() {
		var $tab = $(this);
		var $modal = $tab.closest('.et-tb-divi-library-modal');
		var $modalTabContents = $modal.find('.et-common-divi-library__container:first');
        
        var $dataKey = $tab.attr('data-key');
        if (typeof $dataKey !== typeof undefined && $dataKey !== false) {
		
            if ($dataKey === 'ags-layouts') {
                $modalTabContents.children().hide();
                var $container = $('<div>').addClass('ags-layouts-dialog-container').appendTo($modalTabContents);
                ags_layout_import_ui('divi', $container, 'layouts_ags', null, 'replace');
            } else if ($dataKey === 'ags-layouts-my') {
                $modalTabContents.children().hide();
                var $container = $('<div>').addClass('ags-layouts-dialog-container').appendTo($modalTabContents);
                ags_layout_import_ui('divi', $container, 'layouts_my', null, 'replace');
            } else {
                $modalTabContents.children('.ags-layouts-dialog-container').remove();
                $modalTabContents.children().show();
            }
        }
	});
	
	ags_layouts_onCreateElementWithId('et-fb-settings-column', function(element) {
		var $element = $(element);
		if ($element.hasClass('et-fb-tooltip-modal--save_to_library')) {
			var $nameOption = $element.find('[name=\'template_name\']').closest('.et-fb-settings-option');
			var $saveButton = $element.find('.et-fb-save-library-button:not(.ags-layouts-export-button):first').click(function() {
				if ($saveButton.hasClass('ags-layouts-export-button')) {
					document.cookie = 'ags_layouts_divi_ready=0;path=/';
					var cookieCheckCount = 0;
					var cookieCheckInterval = setInterval(function() {
						if (document.cookie.indexOf('ags_layouts_divi_ready=1') != -1) {
							clearInterval(cookieCheckInterval);
							document.cookie = 'ags_layouts_divi_ready=0;path=/';
							
							$.post(ags_layouts_api_url, {action: 'ags_layouts_get_temp_layout_contents'}, function(layoutContents) {
								if (layoutContents && layoutContents.success && layoutContents.data && layoutContents.data.name && layoutContents.data.contents) {
								
									if (isThemeBuilder) {
										var fbApp = document.getElementById('et-fb-app'); // latest
										var frame = $(fbApp).find('iframe:first');
										
										// This still needs to be fleshed out
										if ( frame.length && frame[0].contentWindow && frame[0].contentWindow.ETBuilderBackendDynamic) {// all good
											window.ags_layouts_admin_config.editingPostUrl = null; // all good 2
										}
										
									}
								
									ags_layout_export_ui('divi', layoutContents.data.contents, null, null, null, layoutContents.data.name,
										layoutContents.data.isFullPage
											? {
												fullPageId: window.ags_layouts_admin_config.editingPostId
											}
											: null
									);
								}
							}, 'json');
						}
						
						++cookieCheckCount;
						if (cookieCheckCount == 40) {
							clearInterval(cookieCheckInterval);
						}
					}, 250);
				}
			});
			var $agsLayoutsOption = $nameOption.clone();
			$agsLayoutsOption.find('label:first').text('WP Layouts:');
			$('<div>')
				.addClass('et-fb-multiple-checkboxes-wrap')
				.append(
					$('<p>')
						.append(
							$('<label>')
								.text('Save to My WP Layouts for use on other sites')
								.prepend(
									$('<input>')
										.attr({
											type: 'checkbox',
											name: 'destination_ags_layouts',
										})
										.change(function() {
											var isChecked = $(this).is(':checked');
											$element.find('[name^=\'et_fb_multiple_checkboxes\']').closest('.et-fb-settings-option').toggle(!isChecked);
											var $newCategoryNameField = $element.find('[name=\'new_category_name\']').val(isChecked ? '__AGS_LAYOUTS__' : '').focus().blur();
											
											if (isChecked) {
												$saveButton
													.addClass('ags-layouts-export-button')
													.data('ags-layouts-original-content', $saveButton.html())
													.text('Upload to My WP Layouts');
											} else {
												$saveButton
													.removeClass('ags-layouts-export-button')
													.html($saveButton.data('ags-layouts-original-content'));
											}
											
											$newCategoryNameField.closest('.et-fb-settings-option').toggle(!isChecked);
											
										})
								)
						)
					
				)
				.replaceAll($agsLayoutsOption.find('input:first'));
			
			$agsLayoutsOption.insertAfter($nameOption);
		}
	});
	

}); // end document ready