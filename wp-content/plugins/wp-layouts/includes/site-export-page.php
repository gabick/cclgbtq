<?php
AGSLayouts::VERSION; // Access control

include_once('site-export/caldera.php');

?>
<div id="ags-layouts-settings">
	<div id="ags-layouts-settings-header">
		<h1>WP Layouts Site Exporter</h1>
		<div id="ags-layouts-settings-header-links">
			<a id="ags-layouts-settings-header-link-settings" href="">Settings</a>
			<a id="ags-layouts-settings-header-link-support" href="https://support.wplayouts.space/" target="_blank">Support</a>
		</div>
	</div>
	<div id="ags-layouts-site-export">
		<form method="post">
			<label>
				Export Name
				<input id="ags-layouts-site-export-name" type="text" value="<?php echo esc_attr( get_bloginfo() );  ?>" required>
			</label>
			
			<label>
				Export Image (optional; PNG format, recommended size 900x750)
				<input id="ags-layouts-site-export-image" type="file">
			</label>
			
			<ul id="ags-layouts-site-export-items">
				<li>
					<label>
						<input type="checkbox" name="exportItems[]" value="content" checked disabled>
						Site content (includes pages, posts, categories, menus, etc.)
					</label>
					
					
				<ul id="ags-layouts-site-export-content-exclude">
					<li>
						<label class="full-size">
							Exclude post types (comma separated):
							<input type="text" name="excludePostTypes" value="">
						</label>
					</li>
					<li>
						<label class="full-size">
							Exclude taxonomies (comma separated):
							<input type="text" name="excludeTaxonomies" value="module_width,scope,layout_type">
						</label>
						
					</li>
				</ul>
				</li>
				<li>
					<label>
						<input type="checkbox" name="exportItems[]" value="themeOptions" <?php echo( in_array( get_option('template'), array('Divi', 'Extra') ) ? 'checked' : 'disabled' ); ?>>
						Theme options
					</label>
					
					<p class="ags-layouts-site-export-instructions">Only Divi and Extra are supported for theme options export in this beta version of the site exporter. If you would like us to support a different theme, please let us know!</p>
				</li>
                <li>
                    <label>
                        <input type="checkbox" name="exportItems[]" value="widgets" checked>
                        Widgets
                    </label>
                </li>
				<li>
					<label>
						Plugin options
					</label>
					
					<ul>
						<li>
							<label>
								<input type="checkbox" name="exportItems[]" value="pluginOptions.TheEventsCalendar" <?php echo( get_option('tribe_events_calendar_options') ? 'checked' : 'disabled' ); ?>>
								The Events Calendar
							</label>
						</li>
					</ul>
					
					<p class="ags-layouts-site-export-instructions">We are planning to add support for more plugins as we improve this beta version of the site exporter. Please let us know if you need support for a particular plugin!</p>
				</li>
				<?php if ( AGSLayoutsSiteExportCalderaForms::isSupported() ) { ?>
				<li>
					<label>
						Caldera Forms
					</label>
					
					<ul>
						<?php foreach ( AGSLayoutsSiteExportCalderaForms::getForms() as $formId => $formName) { ?>
						<li>
							<label>
								<input type="checkbox" name="exportItems[]" value="calderaForms.<?php echo esc_attr($formId); ?>" checked>
								<?php echo esc_html($formName); ?>
							</label>
						</li>
						<?php } ?>
					</ul>
					
				</li>
				
				<?php } ?>
			</ul>
			
			<div id="ags-layouts-site-export-status"></div>
			
			<?php wp_nonce_field('ags-layouts-site-export-ajax', 'ags_layouts_nonce'); ?>
		</form>
	</div>
</div>


<script>
jQuery(document).ready(function($) {
	var $form = $('#ags-layouts-site-export form');
	var $status = $('#ags-layouts-site-export-status');
	
	// wp-layouts\includes\site-export-packager-page.php
	var errorHandler = function() {
		console.log( 'Site export error' + (arguments.length > 1 ? ': ' + arguments[1] : '') );
		if (arguments.length > 2) {
			console.log(arguments[2]);
		}
		
		ags_layouts_message_dialog(
			'Error',
			'Something went wrong during the export.',
			'O'
		);
	};

	var dialogOptions = {
		title: 'WP Layouts Site Exporter',
		container: $form.parent(),
		content: $form,
		pageName: 'site-export',
		firstButtonClass: 'aspengrove-btn-secondary',
		buttons: {
			'Export Site': function() {
		
				var layoutName = $('#ags-layouts-site-export-name').val();
				var layoutData = {
					contents: '',
					extraData: {}
				};
				var tasks = [];
				var selectedTasks = {};
				var screenshotData = -1;
				
				$('#ags-layouts-site-export-items input:checked').each(function() {
					var checkedItem = $(this).val().split('.');
					if (checkedItem.length == 1) {
						selectedTasks[ checkedItem[0] ] = true;
					} else if (selectedTasks[ checkedItem[0] ] && selectedTasks[ checkedItem[0] ].length) {
						selectedTasks[ checkedItem[0] ].push(checkedItem[1]);
					} else {
						selectedTasks[ checkedItem[0] ] = [ checkedItem[1] ];
					}
				});
				
				//if ( selectedTasks.content ) {
					tasks.push([
						'Exporting content...',
						function(cb) {
							$.get(
								'export.php',
								{
									download: true,
								},
								function(response) {
								
									var excludePostTypes = $('#ags-layouts-site-export [name="excludePostTypes"]:first').val();
									if (excludePostTypes) {
										excludePostTypes = excludePostTypes.split(',');
										for (var i = 0; i < excludePostTypes.length; ++i) {
											
											excludePostTypes[i] = excludePostTypes[i].trim();
											$(response).find('wp\\:post_type:contains("' + excludePostTypes[i] + '")').each(function() {
												var $item = $(this);
												if ( $item.text() == excludePostTypes[i] ) {
													$item.parent().remove();
												}
											});
											
										}
										
									}
								
								
									var excludeTaxonomies = $('#ags-layouts-site-export [name="excludeTaxonomies"]:first').val();
									if (excludeTaxonomies) {
										excludeTaxonomies = excludeTaxonomies.split(',');
										for (var i = 0; i < excludeTaxonomies.length; ++i) {
											
											excludeTaxonomies[i] = excludeTaxonomies[i].trim();
											$(response).find('wp\\:term_taxonomy:contains("' + excludeTaxonomies[i] + '")').each(function() {
												var $item = $(this);
												
												if ( $item.text() == excludeTaxonomies[i] ) {
													$item.parent().remove();
												}
											});
											
										}
										
									}
									
									
									
									layoutData.contents = response.documentElement.outerHTML;
									
									cb();
								}
							)
							.fail(errorHandler);
						}
					]);
				//}
				
				if ( selectedTasks.widgets ) {
					tasks.push([
						'Exporting widgets...',
						function(cb) {
							$.get(
								ajaxurl,
								{
									action: 'ags_layouts_get_widgets_export',
									ags_layouts_nonce: $('#ags-layouts-site-export #ags_layouts_nonce').val(),
								},
								function(response) {
									layoutData.extraData.widgets = response;
									cb();
								},
								'text'
							)
							.fail(errorHandler);
						}
					]);
				}
				
				if ( selectedTasks.themeOptions || selectedTasks.pluginOptions ) {
					tasks.push([
						'Exporting theme and/or plugin options...',
						function(cb) {
							$.post(
								ajaxurl,
								{
									action: 'ags_layouts_get_theme_plugin_options_export',
									ags_layouts_nonce: $('#ags-layouts-site-export #ags_layouts_nonce').val(),
									themeOptions: selectedTasks.themeOptions ? true : false,
									pluginOptions: selectedTasks.pluginOptions ? selectedTasks.pluginOptions : null
								},
								function(response) {
									layoutData.extraData.agsxto = response;
									cb();
								},
								'text'
							)
							.fail(errorHandler);
						}
					]);
				}
				
				if ( selectedTasks.calderaForms ) {
					tasks.push([
						'Exporting Caldera Forms...',
						function(cb) {
							$.post(
								ajaxurl,
								{
									action: 'ags_layouts_get_caldera_forms_export',
									ags_layouts_nonce: $('#ags-layouts-site-export #ags_layouts_nonce').val(),
									formIds: selectedTasks.calderaForms
								},
								function(response) {
									layoutData.extraData.caldera_forms = response;
									cb();
								},
								'text'
							)
							.fail(errorHandler);
						}
					]);
				}
				
				tasks.push([
						'Exporting menu assignments...',
						function(cb) {
							$.post(
								ajaxurl,
								{
									action: 'ags_layouts_get_menu_assignments_export',
									ags_layouts_nonce: $('#ags-layouts-site-export #ags_layouts_nonce').val(),
								},
								function(response) {
									layoutData.extraData.config = {};
									
									if (response) {
										layoutData.extraData.config.menus = response;
									}
									
									// below string from my WordPress export XML file, modified the type
									if (layoutData.contents.indexOf('<wp:post_type><![CDATA[et_template]]></wp:post_type>') != -1) {
										layoutData.extraData.config.hasThemeBuilderTemplates = true;
									}
									
									layoutData.extraData.config = JSON.stringify(layoutData.extraData.config);
									
									cb();
								},
								'json'
							)
							.fail(errorHandler);
						}
					]);
				
				tasks.push([
					'Processing image...',
					function(cb) {
						
						// The following code is copied from https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL and modified. Original code is in the public domain.
						  const file = $('#ags-layouts-site-export-image')[0].files[0];
						  const reader = new FileReader();

						  reader.addEventListener("load", function () {
							screenshotData = reader.result;
							cb();
						  }, false);

						  if (file) {
							reader.readAsDataURL(file);
						  } else {
							cb();
						  }
						// End code is copied from https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL and modified.
						
						
					}
				]);
				
				tasks.push([
					'Preparing to upload...',
					function() {
						
						ags_layouts_export_job_ui(
							layoutData.contents,
							screenshotData,
							'site-importer',
							layoutName,
							dialog,
							$form,
							dialog.setButtons,
							dialog.close,
							layoutData.extraData
						);
					}
				]);
				
				function runTask() {
					if (tasks.length) {
						var task = tasks.shift();
						$status.text(task[0]);
						task[1](runTask);
					}
				}
				runTask();
				
				return false;
			}
			
		}
	};
	
	dialog = agsLayoutsDialog.create(dialogOptions);
	
	
});
</script>