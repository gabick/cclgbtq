<?php
AGSLayouts::VERSION; // Access control

include_once(__DIR__.'/account.php');
if (!empty($_POST['ags_layouts_action'])) {
	check_admin_referer('ags_layouts_settings_action', 'ags_layouts_nonce');

	switch ($_POST['ags_layouts_action']) {
		case 'login':
			if (!empty($_POST['ags_layouts_email']) && !empty($_POST['ags_layouts_password'])) {
			
				$loginResult = AGSLayoutsAccount::login(
								sanitize_text_field( wp_unslash( $_POST['ags_layouts_email'] ) ),
								sanitize_text_field( wp_unslash( $_POST['ags_layouts_password'] ) )
				);
				if ($loginResult) {
					$message = 'You have been logged in successfully!';
				} else {
					switch ( AGSLayoutsAccount::getLastLoginError() ) {
						case 'auth':
							$message = 'Your email and/or password is incorrect; <a href="https://wplayouts.space/reset-password/" target="_blank">click here</a> to reset your password. If you are receiving this message in error, please <a href="https://support.wplayouts.space/" target="_blank">contact support</a>.';
							break;
						case 'license_key_add_site':
							$message = 'This site could not be activated in your account. You may have reached your site activation limit, in which case you would need to log out in WP Layouts on one of your other sites before logging in here. If you think this message is in error, please <a href="https://support.wplayouts.space/" target="_blank">contact support</a>.';
							break;
						case 'no_license_key':
							$message = 'We couldn\'t find an active WP Layouts plan in your account. Please ensure you are using the latest version of the WP Layouts plugin, and <a href="https://support.wplayouts.space/" target="_blank">contact support</a> if you are still seeing this message.';
							break;
						default:
							$message = 'Login failed; please try again. If you continue to receive this message, please <a href="https://support.wplayouts.space/" target="_blank">contact support</a>.';
					}
				}
				
				
				
				$messageType = $loginResult ? 'success' : 'error';
			}
			break;
		case 'logout':
			$logoutResult = AGSLayoutsAccount::logout();
			$message = $logoutResult
						? 'You have been logged out.'
						: 'An error occurred while logging out. Please try navigating to Layouts > Layouts to confirm whether you are still logged in on this site. You may also need to manually deactivate this site by logging in to your account on our website.';
			$messageType = $logoutResult ? 'success' : 'error';
			break;
	}
}

$isLoggedIn = AGSLayoutsAccount::isLoggedIn();
if (empty($message)) {
	$message = $isLoggedIn
				? 'You are currently logged in with the email address shown below.'
				: 'You are currently not logged in. To create a new account, <a href="https://wplayouts.space/checkout?edd_action=add_to_cart&download_id=1314" target="_blank">click here</a>.'
					.( AGSLayouts::getThemeDemoData() ? ' You do not need an account to import your theme\'s demo data; <a href="'.esc_url( admin_url( 'admin.php?page=ags-layouts-demo-import' ) ).'">click here</a> to go to the Import Demo Data page.' : '');
	
	$messageType = 'info';
}

?>
<br><p class="ags-layouts-notification ags-layouts-notification-info"><strong>Thank you for being part of the WP Layouts Beta!</strong> We appreciate your patience if you encounter any problems with this product. Please visit our <a href="https://support.wplayouts.space/" target="_blank">support site</a> for tutorials, FAQs, and to contact us.</p>

<div id="ags-layouts-settings">
	<div id="ags-layouts-settings-header">
		<h1>WP Layouts Beta</h1>
		<div id="ags-layouts-settings-header-links">
			<a id="ags-layouts-settings-header-link-settings" href="">Settings</a>
			<a id="ags-layouts-settings-header-link-support" href="https://support.wplayouts.space/" target="_blank">Support</a>
		</div>
	</div>
	<ul id="ags-layouts-settings-tabs">
		<li><a href="#account">Account</a></li>
	</ul>
	<div id="ags-layouts-settings-account">
		<p class="ags-layouts-notification ags-layouts-notification-<?php echo($messageType); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- fixed value (see above) ?>">
			<?php echo($message); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- fixed value (see above) ?>
		</p>
		<form method="post">
			<label>
				<span>Email:</span>
				<input type="email" name="ags_layouts_email" value="<?php echo(esc_attr(AGSLayoutsAccount::getAccountEmail())); ?>">
			</label><br>
			<?php if ($isLoggedIn) { ?>
			<button name="ags_layouts_action" class="aspengrove-btn-secondary" value="logout">Logout</button>
			<?php } else { ?>
			<label>
				<span>Password:</span>
				<input type="password" name="ags_layouts_password">
			</label><br>
			<div id="ags-layouts-login-buttons">
				<a id="ags-layouts-password-reset-link" href="https://wplayouts.space/reset-password/" target="_blank">Reset your password</a>
				<button name="ags_layouts_action" class="aspengrove-btn-primary" value="login">Login</button>
			</div>
			<?php } ?>
			
			<?php wp_nonce_field('ags_layouts_settings_action', 'ags_layouts_nonce'); ?>
		</form>
	</div>
</div>