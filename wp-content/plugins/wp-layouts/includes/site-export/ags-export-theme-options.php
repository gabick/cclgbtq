<?php
/*
AGS Export Theme Options plugin - internal plugin to export theme options to a file when creating child theme demo data
Copyright (C) 2020  Aspen Grove Studios

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
define('AGSXTO_DEBUG', false);

function ags_layouts_xto_export( $themeOptions=true, $pluginOptions=array() ) {
	global $pagenow;

	// Options to export
	$options = array();
	
	if ($themeOptions) {
		// Theme
		$agsxto_theme = get_option('template');
		$agsxto_theme_child = get_option('stylesheet');
	
		if (!in_array($agsxto_theme, array('Divi', 'Extra'))) {
			wp_die('The current theme is not supported for options export.');
		}

		$options['theme_mods_'.$agsxto_theme_child] = array(
			'exclude' => array('sidebars_widgets'), 
			//'missing_delete' => array('divi_main_accent_color','divi_second_accent_color','divi_child_header_font_color','divi_child_body_font_color',)
		);
		$options['et_'.strtolower($agsxto_theme)] = array(
			//'missing_delete' => array('primary_nav_dropdown_line_color','primary_nav_dropdown_link_color',)
		);
	}
	
	
	if ($pluginOptions) {
		
		foreach ( (array) $pluginOptions as $plugin ) {
			switch ($plugin) {
				case 'TheEventsCalendar':
					$options['tribe_events_calendar_options'] = array('google_maps_js_api_key','last-update-message-the-events-calendar');
					break;
			}
		}
		
	}
	
	// Variables to substitute in option values: string => variable name
	$variables = array(
		get_option('siteurl') => 'siteurl' // Trailing slash is automatically removed by WP in get_option()
	);

	$export = array();
	foreach ($options as $option => $optionParams) {
		$exportOption = get_option($option);
		
		if (isset($optionParams['exclude'])) {
			foreach ($exclude as $subOption) {
				unset($exportOption[$subOption]);
			}
		}
		
		if ($option == 'theme_mods_'.$agsxto_theme_child && isset($exportOption['nav_menu_locations'])) {
			foreach ($exportOption['nav_menu_locations'] as $locationName => $locationMenu) {
				if (!empty($locationMenu)) {
					$locationMenu = wp_get_nav_menu_object($locationMenu);
					$exportOption['nav_menu_locations'][$locationName] = (empty($locationMenu->slug) ? '' : $locationMenu->slug);
				}
			}
		}
		
		// Substitute variables
		$export[$option] = array('value' => ags_layouts_xto_substitute_variables($exportOption, $variables));
		
		if (!empty($optionParams['missing_delete'])) {
			$delete = array();
			foreach ($optionParams['missing_delete'] as $optionField) {
				if (!isset($export[$option]['value'][$optionField])) {
					$delete[] = $optionField;
				}
			}
			if (!empty($delete)) {
				$export[$option]['delete'] = $delete;
			}
		}
		
	}
	
	header('Content-Type: text/plain');
	if (AGSXTO_DEBUG) {
		print_r($export);
	} else {
		header('Content-Disposition: attachment; filename="theme_options.dat"');
		echo( base64_encode( serialize($export) ) );
	}
	exit;
}

function ags_layouts_xto_substitute_variables($options, $variables) {
	foreach ($options as $key => $value) {
		if (is_array($value)) {
			$options[$key] = ags_layouts_xto_substitute_variables($value, $variables);
		} else if (is_string($value)) {
			foreach ($variables as $string => $variableName) {
				$options[$key] = str_replace($string, '{{ags.'.$variableName.'}}', $value);
			}
		}
	}
	return $options;
}
