<?php
AGSLayouts::VERSION; // Access control

?>
<div id="ags-layouts-settings">
	<div id="ags-layouts-settings-header">
		<h1>WP Layouts Site Export Packager</h1>
		<div id="ags-layouts-settings-header-links">
			<a id="ags-layouts-settings-header-link-settings" href="">Settings</a>
			<a id="ags-layouts-settings-header-link-support" href="https://support.wplayouts.space/" target="_blank">Support</a>
		</div>
	</div>
	<div id="ags-layouts-site-export-packager">
		<form method="post">
			<div class="hidden">
				Mode:
				<select name="mode" required>
					<option value="standalone">Standalone</option>
					<option value="hook"<?php if (isset($_GET['mode']) && $_GET['mode'] == 'hook') echo(' selected'); ?>>Hook</option>
				</select>
			</div>
			
            <div id="ags-layouts-site-export-packager-export">
                <label>Site exports:</label>
                <ol id="ags-layouts-site-export-packager-export-list">
                </ol>
                <button id="ags-layouts-site-export-packager-export-add" class="aspengrove-btn-add" type="button">Add Export</button>
            </div>
			<label class="ags-layouts-standalone-only ags-layouts-required">
				Menu item name:
				<input name="menuName" type="text" value="Import Demo Data" required>
			</label>
			<label class="ags-layouts-standalone-only ags-layouts-required">
				Menu item parent slug:
				<input name="menuParent" type="text" value="" required>
			</label>
			<label class="ags-layouts-standalone-only ags-layouts-required">
				Class prefix:
				<input name="classPrefix" type="text" value="MyTheme_" required>
			</label>
			<label class="ags-layouts-standalone-only ags-layouts-required">
				Import page title:
				<input name="pageTitle" type="text" value="Import Demo Data" required>
			</label>
			<label>
				Introductory content - import preview/selection screen:
				<?php
				wp_editor(
					'<p>Select which version of the demo data you would like to import, then click Continue.</p>',
					'ags-layouts-site-export-packager-step1Html',
					array('media_buttons' => false,  'textarea_rows' => 2)
				);
				?>
			</label>
			<label>
				Introductory content - import options screen:
				<?php
				wp_editor(
					'<p>Choose which items you would like to import.</p>',
					'ags-layouts-site-export-packager-step2Html',
					array('media_buttons' => false,  'textarea_rows' => 2)
				);
				?>
			</label>
			<label>
				Import button text:
				<input name="buttonText" type="text" value="Import Demo Data" required>
			</label>
			<label>
				Import progress heading:
				<input name="progressHeading" type="text" value="Importing Demo Data..." required>
			</label>
			<label>
				Import complete heading:
				<input name="completeHeading" type="text" value="Import Complete!" required>
			</label>
			<label>
				Import successful message:
				<?php
				wp_editor(
					'<p>Enjoy your new child theme!<br>Please check to make sure that the import was successful.</p>',
					'ags-layouts-site-export-packager-successHtml',
					array('media_buttons' => false,  'textarea_rows' => 2)
				);
				?>
			</label>
			<label>
				Import error message:
				<?php
				wp_editor(
					'<p>Something went wrong.<br>Unfortunately, demo data import could not be completed due to an error. Please try again.</p>',
					'ags-layouts-site-export-packager-errorHtml',
					array('media_buttons' => false,  'textarea_rows' => 2)
				);
				?>
			</label>
			
			<button id="ags-layouts-site-export-packager-button" class="aspengrove-btn-secondary">Create Package</button>
		</form>
		
		<div id="ags-layouts-site-export-packager-instructions-standalone" class="hidden">
			<p>
			After clicking Create Package, a zip file will be created containing the code and other resources for the data importer.
			To add the importer to your child theme, unzip the package zip file into a directory within the child theme (this
			directory should not contain any other files). Then add the following line of code to the end of your theme's functions.php
			file, replacing /path/to/importer/ with the path to the directory containing the package files, relative to the directory
			containing the functions.php file.
			</p>
			<code>include_once __DIR__.'/path/to/importer/importer.php';</code>
		</div>
		
		<div id="ags-layouts-site-export-packager-instructions-hook" class="hidden">
			<p>
			Add the following code to your theme's functions.php file:
			</p>
			<pre><code></code></pre>
		</div>
		
	</div>
</div>
	
<form method="post">
	<input name="action" value="ags_layouts_package" type="hidden">
	<input id="ags-layouts-site-export-package" name="package" type="hidden">
	<?php wp_nonce_field('ags-layouts-site-export-package', 'ags_layouts_nonce'); ?>
</form>

<script>
jQuery(document).ready(function($) {
	var $form = $('#ags-layouts-site-export-packager form');
	var $list = $('#ags-layouts-site-export-packager-export-list');
	
	$('#ags-layouts-site-export-packager-export-add').click(function() {
		ags_layout_import_ui('site-importer', null, 'layouts_my', function(layout) {
			$('<li>')
				.append($('<span>').text(layout.layoutName))
				.append(
					$('<a>')
						//.text('x')
                        .attr('class', 'remove-item')
						.attr('aria-label', 'Remove ' + layout.layoutName)
						.attr('href', '#')
						.click(function() {
							$(this).parent().remove();
							return false;
						})
				)
				.append($('<input name="layoutIds[]" type="hidden">').val(layout.layoutId))
				.appendTo($list);
		}, 'layout-callback');
	});
	
	$('#ags-layouts-site-export-packager [name="mode"]:first').change(function() {
		var mode = $(this).val();
		var enabledSelector = '.ags-layouts-' + mode + '-only';
		var disabledSelector = '.ags-layouts-' + ( mode == 'standalone' ? 'hook' : 'standalone' ) + '-only';
		
		$(enabledSelector).show();
		$(enabledSelector + '.ags-layouts-required :input').attr('required', true);
		
		$(disabledSelector).hide();
		$(disabledSelector + '.ags-layouts-required :input').attr('required', false);
	}).change();
	
	$form.submit(function() {
		$('#ags-layouts-site-export-packager-button').attr('disabled', true);
		
		$('#ags-layouts-site-export-packager-instructions-standalone, #ags-layouts-site-export-packager-instructions-hook').addClass('hidden');
		
		
		var layouts = {};
		var layoutIds = [];
		
		$('#ags-layouts-site-export-packager [name="layoutIds[]"]').each(function() {
			layoutIds.push( $(this).val() );
		});
		if (layoutIds.length) {
			
			var errorHandler = function() {
				ags_layouts_message_dialog(
					'Error',
					'Something went wrong while packaging the site export.',
					'O'
				);
				
				$('#ags-layouts-site-export-packager-button').attr('disabled', false);
			};
			
			function getLayoutData() {
				var layoutId = layoutIds.shift().trim();
				$.post(ags_layouts_api_url, {
					action: 'ags_layouts_get_read_key',
					layoutId: layoutId
				}, function(response) {
					if (
						response.success && response.data
						&& response.data.layoutId && response.data.layoutId == layoutId
						&& response.data.layoutName && response.data.key 
					) {
						layouts[layoutId] = {
							name: response.data.layoutName,
							key: response.data.key
						};
						if (layoutIds.length) {
							getLayoutData();
						} else {
							submitPackage();
							$('#ags-layouts-site-export-packager-button').attr('disabled', false);
						}
						
					} else {
						errorHandler();
					}
				}, 'json')
				.fail(errorHandler);
			}
			getLayoutData();
			
			function submitPackage() {
				var mode = $('#ags-layouts-site-export-packager [name="mode"]:first').val();
			
				var packageData = {
					'layouts': layouts,
					'mode': mode,
					'editor': 'SiteImporter',
					'menuName': ( mode == 'standalone' ? $('#ags-layouts-site-export-packager [name="menuName"]:first').val() : '_na_' ),
					'menuParent': ( mode == 'standalone' ? $('#ags-layouts-site-export-packager [name="menuParent"]:first').val() : '_na_' ),
					'pageTitle': ( mode == 'standalone' ? $('#ags-layouts-site-export-packager [name="pageTitle"]:first').val() : '_na_' ),
					'classPrefix': ( mode == 'standalone' ? $('#ags-layouts-site-export-packager [name="classPrefix"]:first').val() : '_na_' ),
					'step1Html': wp.editor.getContent('ags-layouts-site-export-packager-step1Html'),
					'step2Html': wp.editor.getContent('ags-layouts-site-export-packager-step2Html'),
					'buttonText': $('#ags-layouts-site-export-packager [name="buttonText"]:first').val(),
					'progressHeading': $('#ags-layouts-site-export-packager [name="progressHeading"]:first').val(),
					'completeHeading': $('#ags-layouts-site-export-packager [name="completeHeading"]:first').val(),
					'successHtml': wp.editor.getContent('ags-layouts-site-export-packager-successHtml'),
					'errorHtml': wp.editor.getContent('ags-layouts-site-export-packager-errorHtml'),
				};
				
				switch (mode) {
					case 'standalone':
						$('#ags-layouts-site-export-package')
							.val( JSON.stringify(packageData) )
							.parent()
							.attr('action', ajaxurl)
							.submit();
						$('#ags-layouts-site-export-packager-instructions-standalone').removeClass('hidden');
						break;
					case 'hook':
						$.post(ajaxurl, {
							action: 'ags_layouts_package',
							ags_layouts_nonce: $('#ags_layouts_nonce').val(),
							'package': JSON.stringify(packageData)
						}, function(response) {
							if ( response.success && response.data ) {
								$('#ags-layouts-site-export-packager-instructions-hook code:first').text(response.data);
								$('#ags-layouts-site-export-packager-instructions-hook').removeClass('hidden');
							} else {
								errorHandler();
							}
						}, 'json')
						.fail(errorHandler);
						
						break;
				}
				
				
			}
			
			
		} else {
			ags_layouts_message_dialog(
				'No site exports selected',
				'Please select at least one site export to include in the package.',
				'O'
			);
			$('#ags-layouts-site-export-packager-button').attr('disabled', false);
		}
		
		return false;
	});
});
</script>