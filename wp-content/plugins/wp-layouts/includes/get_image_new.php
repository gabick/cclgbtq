<?php
AGSLayouts::VERSION; // Access control

class AGSLayoutsGetImage {
	private static $curl;
	
	public static function run() {
		//header('Content-Type: text/javascript');
		if (empty($_GET['layoutId']) || !is_numeric($_GET['layoutId']) || empty($_GET['image'])) {
			return;
		}
		include_once(__DIR__.'/account.php');
		self::$curl = curl_init(AGSLayouts::API_URL
								.'?action=ags_layouts_get_layout_image&_ags_layouts_token='.urlencode(AGSLayoutsAccount::getToken())
								.'&_ags_layouts_site='.urlencode(get_option('siteurl'))
								.'&layoutId='.$_GET['layoutId']
								.'&imageFile='.urlencode($_GET['image']));
		
		$isLayoutImage = $_GET['image'] == 'L';
		if ($isLayoutImage) {
			curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, true); // checked
		}
		
		$curlReturn = curl_exec(self::$curl);
		
		if ($isLayoutImage && $curlReturn) {
			echo($curlReturn);
			AGSLayoutsImageCache::store($imageId, $curlReturn);
		}
		
	}
}
AGSLayoutsGetImage::run();

/*
Image caching strategy:
- Decided single file image cache is the best vs. multi-file or hybrid file/mysql; also looked for existing solutions
- Cache index at top of file consists of pairs of fixed length fields; each pair represents the ID and offset of one image; number of pairs is fixed to the max cache capacity
- On read, the corresponding cache index pair is moved to the beginning of the cache index
- When adding to cache, if cache size would exceed the max capacity, cache is trimmed to the min capacity by removing images at the end of the cache index

*/

class AGSLayoutsImageCache {
	const	CACHE_FILE_HEADER = '<?php exit; ?>',
			CACHE_CAPACITY_MIN = 79,
			CACHE_CAPACITY_MAX = 99,
			FIELD_LENGTH_ID = 10,
			FIELD_LENGTH_OFFSET = 10,
			FIELD_LENGTH_LENGTH = 7;
	private $cacheFile;
	private static $headerLength, $cacheOffset, $imageRecordLength;
	
	function __construct() {
		$this->cacheFile = fopen(ABSPATH.'wp-content/uploads/wp-layouts-images.php', 'rw'); // checked for re-do
		self::$imageRecordLength || self::$imageRecordLength = self::FIELD_LENGTH_ID + self::FIELD_LENGTH_OFFSET + self::FIELD_LENGTH_LENGTH;
		self::$headerLength || self::$headerLength = strlen(self::CACHE_FILE_HEADER);
		self::$cacheOffset || self::$cacheOffset = (self::$headerLength + ((self::FIELD_LENGTH_ID + self::FIELD_LENGTH_OFFSET) * CACHE_CAPACITY_MAX));
	}
	
	function outputImage($imageId) {
		flock($this->cacheFile, LOCK_SH);
		
		
		for ($i = self::$headerLength; $i < self::$cacheOffset; $i += self::$imageRecordLength) {
			fseek($this->cacheFile, $i);
			if (fread($this->cacheFile, self::FIELD_LENGTH_ID) == $imageId) {
				$imageOffset = fread($this->cacheFile, self::FIELD_LENGTH_OFFSET);
				$imageLength = fread($this->cacheFile, self::FIELD_LENGTH_LENGTH);
				
				// Move the current record to the front of the cache index, if it isn't there already
				
				
				// Now we need to output the image from imageOffset - see stream_copy_to_stream and STDOUT constant
				
				
			}
		}
		
		
		flock($this->cacheFile, LOCK_UN);
	}
	
	function store($imageId, $imageContent) {
		flock($this->cacheFile, LOCK_EX);
		
		fseek($this->cacheFile, self::$headerLength - self::FIELD_LENGTH_OFFSET - self::FIELD_LENGTH_OFFSET);
		$lastImageOffset = fread($this->cacheFile, self::FIELD_LENGTH_OFFSET);
		$lastImageLength = fread($this->cacheFile, self::FIELD_LENGTH_LENGTH);
		
		for ($i = ; $i < self::$cacheOffset; $i += $imageRecordLength) {
			
			if (fread($this->cacheFile, self::FIELD_LENGTH_ID) == $imageId) {
				
				$nextImageRecordOffset = $i + $imageRecordLength;
				
				// Move the current record to the front of the cache index, if it isn't there already
				
				if ($nextImageRecordOffset < self::$cacheOffset) { // checked <-
					fseek($this->cacheFile, $nextImageRecordOffset);
					$nextImageOffset = fread($this->cacheFile, self::FIELD_LENGTH_OFFSET);
				}
				
				
				// Now we need to output the image from imageOffset to nextImageOffset (if applicable) - see stream_copy_to_stream and STDOUT constant
				
				
			}
		}
		
		
		flock($this->cacheFile, LOCK_UN);
	}
}