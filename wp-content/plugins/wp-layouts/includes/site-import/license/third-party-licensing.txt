The following credits and licensing information apply to the files in the parent directory (../) and any of its subdirectories. See individual code files for modification statements.

Contains code from the Radium One-Click Demo Install project; see radium-license.txt
Used in this project under GNU General Public License version 3 or later (see GPL3.txt)
 
Contains code from the WordPress Importer plugin.
Copyright WordPress.org.
Released under GNU General Public License version 2 or later. Licensed in this project under GNU General Public License version 3 or later (see GPL3.txt).

Contains code based on and/or copied from the Divi theme, copyright Elegant Themes, licensed under GPL version 3 for this project by special permission
(see divi-CREDITS.md for credits applicable to Divi, and GPL3.txt for GPL version 3 text).

Contains code based on and/or copied from WordPress by Automattic, released under GPLv2+, licensed under GPLv3+ (see wp-license.txt for the copyright, license,
and additional credits applicable to WordPress, and GPL3.txt for GPLv3 text).

Contains code from the Widget Importer & Exporter plugin.
See below for copyright and license details (licensed in this project under GPLv3+, see GPL3.txt for GPLv3 text).
@copyright  Copyright (c) 2013 - 2017, ChurchThemes.com
@link       https://churchthemes.com/plugins/widget-importer-exporter/
@license    GPLv2 or later