<?php
require_once( 'code/services-shortcode.php' );
require_once( 'code/added-social-icons.php' );
require_once( 'code/Project-Portfolio/Projet-metabox.php' );
require_once( 'includes/admin-login/admin-login.php' );
function my_enqueue_assets() {
    wp_enqueue_style( 'parent-main', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'gabick-main', '/pub/build/styles/main.css' );
    wp_enqueue_style( 'gabick-copyright', '/pub/build/styles/copyright.css' );
    wp_enqueue_style( 'gabick-mobile', '/pub/build/styles/better-mobile-menu.css' );
    wp_enqueue_style( 'gabick-modules', '/pub/build/styles/modules.css' );
//    wp_enqueue_style( 'mobile-slide-menu', get_stylesheet_directory_uri().'/includes/mobile-menu-slide./css/mobile-menu-slide.css' );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_assets' );

function my_scripts_method()
{
    wp_enqueue_script(
        'nwayo-dependencies',
        '/pub/build/scripts/dependencies-head-sync.js',
        array('jquery'),
        true
    );
    wp_enqueue_script(
        'nwayo-dependencies-sync',
        '/pub/build/scripts/dependencies.js',
        array('jquery'),
        true
    );
    wp_enqueue_script(
        'nwayo-main',
        '/pub/build/scripts/main.js',
        array('jquery'),
        true
    );
    wp_enqueue_script(
        'better-mobile-menu',
        '/pub/build/scripts/better-mobile-menu.js',
        array('jquery'),
        true
    );
    wp_enqueue_script(
        'modules',
        '/pub/build/scripts/modules.js',
        array('jquery'),
        true
    );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_method', 15 );

function my_custom_mime_types( $mimes ) {

// New allowed mime types.
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    $mimes['doc'] = 'application/msword';

// Optional. Remove a mime type.
    unset( $mimes['exe'] );

    return $mimes;
}
add_filter( 'upload_mimes', 'my_custom_mime_types' );

function divi_child_theme_setup() {
    if ( ! class_exists('ET_Builder_Module') ) {
        return;
    }
    get_template_part( 'code/Project-Portfolio/CustomFilterablePortfolio' );
    $customFilterablePortfolio = new Custom_ET_Builder_Module_Portfolio();
    remove_shortcode( 'et_pb_portfolio' );
    add_shortcode( 'et_pb_portfolio', array($customFilterablePortfolio, '_shortcode_callback') );
}

add_action( 'wp', 'divi_child_theme_setup', 9999 );

?>
