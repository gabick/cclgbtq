<?php

/**
 * Adds a metabox to the right side of the screen under the â€œPublishâ€ box
 */
function wpt_add_event_metaboxes() {
    add_meta_box(
        'gabick_project_url',
        'Project Url',
        'gabick_project_url',
        'project',
        'normal',
        'high'
    );

    add_meta_box(
        'gabick_project_address',
        'Project address',
        'gabick_project_address',
        'project',
        'normal',
        'high'
    );
}


/**
 * Output the HTML for the metabox.
 */
function gabick_project_url() {
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field( basename( __FILE__ ), 'event_fields' );
    // Get the project-url data if it's already been entered
    $projectUrl = get_post_meta( $post->ID, 'project-url', true );
    // Output the field
    echo '<input type="text" name="project-url" value="' . esc_textarea( $projectUrl )  . '" class="widefat">';
}

/**
 * Output the HTML for the metabox.
 */
function gabick_project_address() {
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field( basename( __FILE__ ), 'event_fields' );
    // Get the project-url data if it's already been entered
    $projectAddress = get_post_meta( $post->ID, 'project-address', true );
    // Output the field
    echo '<input type="text" name="project-address" value="' . esc_textarea( $projectAddress )  . '" class="widefat">';
}

/**
 * Save the metabox data
 */
function wpt_save_events_meta( $post_id, $post ) {
    // Return if the user doesn't have edit permissions.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }
    // Verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times.
    if ( ! isset( $_POST['project-url'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
        return $post_id;
    }

    if ( ! isset( $_POST['project-address'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
        return $post_id;
    }
    // Now that we're authenticated, time to save the data.
    // This sanitizes the data from the field and saves it into an array $events_meta.
    $events_meta['project-url'] = esc_textarea( $_POST['project-url'] );
    $events_meta['project-address'] = esc_textarea( $_POST['project-address'] );
    // Cycle through the $events_meta array.
    // Note, in this example we just have one item, but this is helpful if you have multiple.
    foreach ( $events_meta as $key => $value ) :
        // Don't store custom data twice
        if ( 'revision' === $post->post_type ) {
            return;
        }
        if ( get_post_meta( $post_id, $key, false ) ) {
            // If the custom field already has a value, update it.
            update_post_meta( $post_id, $key, $value );
        } else {
            // If the custom field doesn't have a value, add it.
            add_post_meta( $post_id, $key, $value);
        }
        if ( ! $value ) {
            // Delete the meta key if there's no value
            delete_post_meta( $post_id, $key );
        }
    endforeach;
}

function project_remove_post_type_support() {
    remove_post_type_support( 'project', 'editor');
    remove_post_type_support( 'project', 'comments');
    remove_post_type_support( 'project', 'author');
    remove_post_type_support( 'project', 'tags');
    remove_post_type_support( 'project', 'custom-fields');
}

add_action('init', 'project_remove_post_type_support');
add_action( 'add_meta_boxes', 'wpt_add_event_metaboxes' );
add_action( 'save_post', 'wpt_save_events_meta', 1, 2 );
